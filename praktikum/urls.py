"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin

from app_home.views import index as index_home
from app_profile.views import index as index_profile
from app_signup.views import index as index_signup

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^home/', include('app_home.urls', namespace='home')),
    re_path(r'^profile/', include('app_profile.urls', namespace='profile')),
    re_path(r'^signup/', include('app_signup.urls', namespace='signup')),
    re_path(r'^$', index_home, name='index')
]
